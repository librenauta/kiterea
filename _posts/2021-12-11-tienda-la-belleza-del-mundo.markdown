---
layout: post-tienda
title: "Belleza"
date: 2021-12-11
description: postales
categories: shop
image: /assets/images/tienda/la-belleza-del-mundo/la-belleza-1.png
images-array:
  - image: assets/images/tienda/la-belleza-del-mundo/la-belleza-1.png
    alt: This is some alt text
  - image: assets/images/tienda/la-belleza-del-mundo/la-belleza-2.png
    alt: This is some alt text
  - image: assets/images/tienda/la-belleza-del-mundo/la-belleza-3.png
    alt: This is some alt text
  - image: assets/images/tienda/la-belleza-del-mundo/la-belleza-4.png
    alt: This is some alt text
  - image: assets/images/tienda/la-belleza-del-mundo/la-belleza-5.png
    alt: This is some alt text
author: Pierina
link-pago: https://mpago.la/2NY7iah
precio: 600
tags:
  - Artwork
  - Kiterea
---
  <div class="col soft mb-5">
    <h1>Belleza</h1>
  </div>
  <div class="row">
    <div class="col-md-6 soft mb-3">
      <p><i>
      La imagen es una creación del espíritu. No puede nacer de una comparación, sino de la aproximación de dos realidades mas o menos alejadas. Cuánto más lejanas y justas sean las relaciones de las dos realidades acercadas, más fuerte será la imagen y poseerá más potencia y realidad poética.</i></p>
      <br>
      <p>Pierre Reverdy.</p>
    </div>

    <div class="col-md-6 soft mb-3">
    <p> Collages realizados con enciclopedias encontradas. Surgen a partir de un ejercicio que consiste en reconocer las relaciones posibles entre lo múltiple y en apariencia dispar. Son imágenes que no parten desde la semejanza entre aquello que se muestra, sino que se componen desde sus intensidades y asociaciones visuales. </p>
    </div>

      <div class="col-md-6 soft mb-5 text-work">

      Fanzine <br>
      14,85 x 21 cm <br
      Tapa: Chamoise crema 280gr <br
      Interior: Obra de caña natural 80gr <br
      14 páginas + papeles intervenidos en serigrafía <br
      Impresión Láser <br
      2021

      </div>

      {% include precio-tienda.html %}
