---
layout: post
title: "Genius Loci"
date: 2020-02-06
description: Flyer
client:
  name: Osmiornica Biblioteca
  link: https://osmiornicabiblioteca.com
categories: work
image: /assets/images/work/work-7.jpg
author: Pierina
tags:
  - Artwork
  - Kiterea
  - Osmiornica
---
