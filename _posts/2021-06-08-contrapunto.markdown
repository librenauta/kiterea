---
layout: post
title: "Contrapunto: literatura japonesa y latinoamericana"
date: 2021-06-06
description: Flyer
client:
  name: Osmiornica biblioteca
  link: https://www.instagram.com/osmiornicabiblioteca/
categories: work
image: /assets/images/work/portfolio-50.png
author: Pierina
tags:
  - Flyer
  - Kiterea
  - Ver y Poder
---
