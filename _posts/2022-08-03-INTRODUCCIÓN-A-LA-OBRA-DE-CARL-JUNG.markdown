---
layout: post
title: "INTRODUCCIÓN A LA OBRA DE CARL JUNG"
date: 2022-08-03
description: Flyer
client:
  name: Espacio anzaldua
  link: https://www.instagram.com/espacioanzaldua/
categories: work
image: /assets/images/work/portfolio-80.gif
author: Pierina
tags:
  - Kiterea
  - Jung
  - espacioanzaldua
---
