---
layout: post
title: "Para: Dolly"
date: 2020-02-16
description: Flyer
client:
  name: Osmiornica Biblioteca
  link: https://osmiornicabiblioteca.com
categories: work
image: /assets/images/work/work-16.jpg
author: Pierina
tags:
  - Artwork
  - Kiterea
---
