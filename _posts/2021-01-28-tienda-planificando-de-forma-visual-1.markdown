---
layout: post-tienda
title: "Planificando de forma visual Notas sobre “cine personal” y “cine industrial”"
subtitle: "Maya Deren"
date: 2020-03-03
description: zine
categories: shop
image: /assets/images/tienda/planificando-de-forma/zine-maya-0.jpg
images-array:
  - image: assets/images/tienda/planificando-de-forma/zine-maya-0.jpg
    alt: This is some alt text
  - image: assets/images/tienda/planificando-de-forma/zine-maya-1.jpg
    alt: This is some alt text
  - image: assets/images/tienda/planificando-de-forma/zine-maya-2.jpg
    alt: This is some alt text
author: Pierina
link-pago: https://mpago.la/2GHXq4f
precio: 600
tags:
  - Artwork
  - Kiterea
---
  <div class="col soft mb-5">
    <h1>Planificando de forma visual Notas sobre “cine personal” y “cine industrial”</h1>
    <h2>Maya Deren</h2>
  </div>
  <div class="row">
    <div class="col-md-6 soft mb-3">
      <p>Fragmento del libro "El universo dereniano. Textos fundamentales de la cineasta Maya Deren" editado por Artea y traducido por Carolina Martínez, que también pueden hallar rápidamente entero en internet. Para todx aquel que se acerque al universo del llamado cine experimental, la obra de Maya Deren resulta indispensable para entender el espíritu amateur y artesanal que inspiró a tantxs. Multifacética y enérgica, en su brevísima vida Maya escribió, produjo, actuó y distribuyó sus películas con la ayuda de unxs pocxs amigxs. Forjó sus propias reglas para narrar con imágenes y sonidos sus mundos interiores, haciendo uso de lo real delante de cámara e inquietándolo con la fuerza de su mirada. Su obra es un legado para todxs quienes seguimos insistiendo en crear mundos audiovisuales pese y con las contingencias del presente.</p>
    </div>

    <div class="col-md-6 soft mb-3">
    <p>
    Eleanora Derenkowsky nació en Ucrania en 1908 y se radicó en Estados Unidos en 1935. Fue bailarina, coreógrafa, escritora y cineasta. Su apellido se acorta a “Deren” al poco tiempo de que su familia migrara a los Estados Unidos; ella se define como Maya Deren a partir de 1943. Pionera del cine experimental, concibió su obra con la colaboración de unos pocos artistas, entre ellos su segundo marido, el fotógrafo Alexander Hammid. Deren produjo su obra con total independencia, siendo ella la protagonista de algunas de sus películas y utilizando el formato 16mm en blanco y negro -habitualmente destinado para películas documentales- obteniendo como resultado una lírica visual onírica distinta a todo lo exhibido en la época. Deren distribuyó sus propias películas a nivel internacional exhibiéndolas en universidades, museos, festivales y resultando ganadoras de numerosos premios. “Hago mis películas con lo que Hollywood gasta en pintalabios” decía. Falleció a los 44 años habiendo realizado únicamente 7 cortometrajes considerados de enorme importancia para la historia del cine.</p>
    </div>

      <div class="col-md-6 soft mb-5 text-work">

  Fanzine<br>
  14,85 cm x 21 cm<br>
  Impresión láser<br>
  26 páginas<br>
  Español<br>
  2019<br>
  Trabajo en conjunto con <br><a class="a-shadow soft" href="https://www.instagram.com/verypoder/">Azul Aizenberg</a><br>

      </div>

      {% include precio-tienda.html %}
