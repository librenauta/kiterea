---
layout: post
title: "PIEDRA INFINITA"
date: 2021-11-07
description: Afiche
client:
  name: Piedra infinita film
  link: https://www.instagram.com/piedrainfinitafilm
categories: work
image: /assets/images/work/portfolio-70.png
author: Pierina
tags:
  - Kiterea
  - Afiche
---
