---
layout: post-tienda
title: "Afiches Amarillos"
date: 2021-12-12
description: Afiches
categories: shop
image: /assets/images/tienda/afiches-pegatinas/1.png
images-array:
  - image: assets/images/tienda/afiches-pegatinas/1.png
    alt: This is some alt text
  - image: assets/images/tienda/afiches-pegatinas/2.png
    alt: This is some alt text
  - image: assets/images/tienda/afiches-pegatinas/3.png
    alt: This is some alt text
  - image: assets/images/tienda/afiches-pegatinas/4.png
    alt: This is some alt text
  - image: assets/images/tienda/afiches-pegatinas/5.png
    alt: This is some alt text
  - image: assets/images/tienda/afiches-pegatinas/6.png
    alt: This is some alt text
author: Pierina
link-pago: https://mpago.la/1rCcKM2
precio: 600
tags:
  - Artwork
  - Kiterea
---
  <div class="col soft mb-5">
    <h1>Afiches.</h1>
  </div>
  <div class="row">
    <div class="col-md-6 soft mb-3">
      <p>
      "Un niño en la oscuridad, presa del miedo, se tranquiliza canturreando. Camina, camina y se para de acuerdo con su canción. Perdido, se cobija como puede o se orienta a duras penas con su cancioncilla. Esa cancioncilla es como un esbozo de un centro estable y tranquilo, estabilizante y tranquilizante, en el seno del caos. Es muy posible que el niño, al mismo tiempo que canta, salte, acelere o aminore su paso; pero la canción ya es por si misma un salto: salta del caos a un principio de orden en el caos, pero también corre constantemente el riesgo de desintegrarse." </p>
    </div>

    <div class="col-md-6 soft mb-3">
    <p>Las imágenes son collages intervenidos con pegatinas amarillas, realizados con fragmentos de distintos fanzines. </p>
    </div>

      <div class="col-md-6 soft mb-5 text-work">

      Afiches <br>
      21 x 29,7 cm <br>
      Papel Chamoise crema 250gr <br>
      Impresión Láser <br>
      2021<br>

      Una vez realizada la compra nos pondremos en contacto para que especifiques tu elección.
      </div>
      <div>

      </div>

  </div>

  <div class="row">
    {% include precio-tienda.html text="A4" %}
  </div>
