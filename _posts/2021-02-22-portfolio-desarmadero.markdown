---
layout: post
title: "Desarmadero"
date: 2021-02-23
description: Flyer
client:
  name: Osmiornica Biblioteca
  link: https://osmiornicabiblioteca.com
categories: work
image: /assets/images/work/work-23.jpg
author: Pierina
tags:
  - Artwork
  - Kiterea
---
