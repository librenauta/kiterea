---
layout: post-tienda
title: "Qué está mal en ésta imagen?"
subtitle: "Michelle Citron"
date: 2020-03-03
description: zine
categories: shop
image: /assets/images/tienda/que-esta-mal/zine-citron-1.png
images-array:
  - image: assets/images/tienda/que-esta-mal/zine-citron-1.png
    alt: This is some alt text
  - image: assets/images/tienda/que-esta-mal/zine-citron-2.png
    alt: This is some alt text
  - image: assets/images/tienda/que-esta-mal/zine-citron-3.png
    alt: This is some alt text
  - image: assets/images/tienda/que-esta-mal/zine-citron-4.png
    alt: This is some alt text
  - image: assets/images/tienda/que-esta-mal/zine-citron-5.png
    alt: This is some alt text
author: Pierina
link-pago: https://mpago.la/1f7U9K8
precio: 600
tags:
  - Artwork
  - Kiterea
---

  <div class="col soft mb-5">
    <h1>Qué está mal en ésta imagen?</h1>
    <h2>Michelle Citron</h2>
  </div>
  <div class="row">
    <div class="col-md-6 soft">
      <p>
      Cineasta, psicoanalista y docente estadounidense. Este texto pertenece a su libro "Home movies and other necessary fictions", proyecto que le llevó a Michelle Citron diez años de trabajo a través de la investigación, el visionado de las películas familiares filmadas por su padre y la escritura, a la par que continuaba realizando sus películas. Un texto necesario para comprender las potencialidades ocultas de las llamadas home-movies, las películas familiares que contribuyeron a la construcción de un modelo familiar, empezando por las películas rodadas en 16mm por familias adineradas de los años 20, pasando por el super8 y sus variables utilizadas por familias de clase trabajadora de mediados de siglo XX hasta los flamantes colores del video de la última década del ‘90. Una práctica amateur que propone, más allá de la consciencia de quien filma, la producción de lo que la autora llama una “ficción necesaria”, una promesa de felicidad y tranquilidad que no siempre coincide con la mirada adulta que las revisa. El trabajo de la mirada que recupera aquellas imágenes se hace parte de ella, excavando sus capas de sentido, exponiéndolas a la luz del presente.</p>
    </div>
    <div class="col-md-6 soft mb-3">
    <p>
En su prefacio al libro, la autora explica que antes de publicarlo se lo mostró a su padre y a su madre, para que lo lean detenidamente. Se pregunta por las historias que decidió suprimir durante la escritura del libro:<br>
<i>“¿Es que esas historias no eran realmente trascendentes? ¿O estaba protegiendo a mi madre y a mi padre de la misma manera en que ellos me protegieron acerca de mi incesto cuando era chica? ¿Son los silencios de mi escritura equivalentes a los silencios de mi infancia? ¿Es que nunca escapamos a la repetición? Espero que mi escritura pueda hackear las estructuras, que las palabras me liberen de la compulsión de repetir. No puedo ni siquiera saber cuáles son las historias que provocarían el quiebre. Son como minas enterradas bajo un llano a través del cual camino, cuya ubicación sólo puede desencadenarse a través de un paso verbal que las detone."</i></p>
    </div>

      <div class="col-md-6 soft mb-5 text-work">
      Fanzine <br>
      14,85 x 21 cm <br>
      Tapa: Papel texturado beige 200gr <br>
      Interior: Obra de caña natural 80gr <br>
      26 páginas + papeles intervenidos en serigrafía <br>
      Impresión Láser <br>
      Español <br>
      Trabajo en conjunto con<br> <a class="a-shadow soft" href="https://www.instagram.com/verypoder/">Azul Aizenberg</a><br>
      2019
      </div>

      {% include precio-tienda.html %}
