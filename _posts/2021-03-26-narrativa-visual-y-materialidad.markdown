---
layout: post
title: "Narrativa visual y materialidad en fotografía"
date: 2021-03-26
description: Flyer
client:
  name: Osmiornica Biblioteca
  link: https://osmiornicabiblioteca.com
categories: work
image: /assets/images/work/work-28.jpg
author: Pierina
tags:
  - Artwork
  - Kiterea
---
