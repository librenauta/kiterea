---
layout: post-tienda
title: "Postales"
date: 2020-03-03
description: postales
categories: shop
image: /assets/images/tienda/postales/postales-0.jpg
images-array:
  - image: assets/images/tienda/postales/postales-0.jpg
    alt: This is some alt text
  - image: assets/images/tienda/postales/postales-1.jpg
    alt: This is some alt text
  - image: assets/images/tienda/postales/postales-2.jpg
    alt: This is some alt text
  - image: assets/images/tienda/postales/postales-3.jpg
    alt: This is some alt text
  - image: assets/images/tienda/postales/postales-4.jpg
    alt: This is some alt text
author: Pierina
link-pago: https://mpago.la/1FGbTNL
precio: 1100
tags:
  - Artwork
  - Kiterea
---
  <div class="col soft mb-5">
    <h1>Postales</h1>
  </div>
  <div class="row">
    <div class="col-md-6 soft mb-3">
      <p>
      Retomar olvidadas formas de comunicación. Ajustandose a un tiempo desacostumbrado, no inmediato.<br>
      Un mensaje que está contenido y se diga en voz baja, como un secreto</p>
    </div>

    <div class="col-md-6 soft mb-3">
    <p>4 postales con simbolismo universal: la vida y la muerte, la trascendencia y lo perecedero.</p>
    </div>

      <div class="col-md-6 soft mb-5 text-work">

  Postales<br>
  14,85 cm x 21 cm<br>
  4 postales<br>
  Papel Perlado beige 240gr<br>
  Impresión láser<br>
  2020<br>

      </div>

      {% include precio-tienda.html %}
