---
layout: post
title: "Desarmadero"
date: 2020-02-15
description: Flyer
client:
  name: Osmiornica Biblioteca
  link: https://osmiornicabiblioteca.com
categories: work
image: /assets/images/work/work-15.jpg
author: Pierina
tags:
  - Artwork
  - Kiterea
---
