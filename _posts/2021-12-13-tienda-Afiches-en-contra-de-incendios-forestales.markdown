---
layout: post-tienda
title: "Afiches Rojos."
date: 2021-12-13
description: Afiches
categories: shop
image: /assets/images/tienda/afiches-en-contra-de-los-incendios-forestales/0.png
images-array:
  - image: assets/images/tienda/afiches-en-contra-de-los-incendios-forestales/0.png
    alt: This is some alt text
  - image: assets/images/tienda/afiches-en-contra-de-los-incendios-forestales/1.png
    alt: This is some alt text
  - image: assets/images/tienda/afiches-en-contra-de-los-incendios-forestales/2.png
    alt: This is some alt text
  - image: assets/images/tienda/afiches-en-contra-de-los-incendios-forestales/3.png
    alt: This is some alt text
  - image: assets/images/tienda/afiches-en-contra-de-los-incendios-forestales/4.png
    alt: This is some alt text
  - image: assets/images/tienda/afiches-en-contra-de-los-incendios-forestales/5.png
    alt: This is some alt text
author: Pierina
link-pago: https://mpago.la/2Cd3Xio
precio: 600
tags:
  - Artwork
  - Kiterea
---
  <div class="col soft mb-5">
    <h1>Afiches.</h1>
  </div>
  <div class="row">
    <div class="col-md-6 soft mb-3">
      <p>Ilustraciones realizadas con microfibra negra y roja sobre papel ecológico de caña de 80gr.
      Surgen como una forma de protesta ante los incendios forestales que vienen azotando hace
      años nuestro territorio, causando daños irreparables tanto en la flora como en la fauna regional,
      sin una ley que lo contemple. </p>
    </div>

    <div class="col-md-6 soft mb-3">
    <p><i>"Los humedales son parte integral de los sistemas naturales que hacen posible la vida en la
    Tierra. El 40% de la biodiversidad mundial vive o se reproduce en ellos. Son grandes filtros
    depuradores y reservorios de agua dulce. Amortiguan los impactos de las lluvias y almacenan
    más carbono que ningún otro ecosistema, por eso son grandes aliados en la lucha contra el
    cambio climático. Nuestros humedales necesitan una ley de presupuestos mínimos de
    protección ambiental que asegure su conservación y su uso ambientalmente respetuoso,
    conforme el artículo 41 de la Constitución Nacional."</i></p>
    </div>

    <div class="col-md-6 soft mb-3">
    <p>Basta de incendios forestales. Ley de humedales ya.</p>
    </div>

    <div class="col-md-6 soft mb-5 text-work">
    Afiches <br>
    21 x 29,7 cm <br>
    Papel Chamoise crema 250gr <br>
    Impresión Láser<br>
    2021<br>
    <p>Una vez realizada la compra nos pondremos en contacto para que especifiques tu elección.</p>
    </div>

  </div>
<div class="row">
  {% include precio-tienda.html text="A4" %}
</div>
