---
layout: post-tienda
title: "Basura de palacios"
date: 2020-03-03
description: zine
categories: shop
image: /assets/images/tienda/basura/basura-0.png
images-array:
  - image: assets/images/tienda/basura/basura-0.png
    alt: This is some alt text
  - image: assets/images/tienda/basura/basura-1.png
    alt: This is some alt text
  - image: assets/images/tienda/basura/basura-2.png
    alt: This is some alt text
author: Pierina
link-pago: https://mpago.la/1qVXwnq
precio: 600
tags:
  - Artwork
  - Kiterea
---
  <div class="col soft mb-5">
    <h1>Basura de palacios</h1>
  </div>
  <div class="row">
    <div class="col-md-6 soft mb-3">
      <p>
      Los restos olvidados de una lotería clausurada, montañas de residuos provenientes de un jardín escondido en el centro de la ciudad, gusanos que se arrastran por las páginas creando mapas invisibles, containers desplegados por las calles húmedas, peleas con cartoneros por un botín brillante, madrugadas de insomnio.<br>
      Todo lo que provenga de un tiempo inexacto, espectros sobrevivientes que toman formas imprecisas.</p>
    </div>

    <div class="col-md-6 soft mb-3">
    <p>Fanzine creado en el año 2019. Los collages fueron realizados íntegramente con material encontrado en diversas partes de la ciudad de Buenos Aires y alrededores.</p>
    </div>

    <div class="col-md-6 soft mb-5 text-work">
    Fanzine <br>
    14,85 x 21 cm <br>
    Tapa: Cartulina gris / papel texturado beige 200gr<br>
    Interior: Obra de caña natural 80gr <br>
    12 páginas + filminas impresas <br>
    Impresión Láser <br>
    2019
    </div>
  </div>

{% include precio-tienda.html %}
