---
layout: post
title: "¿Será la memoria un archivo infinito?"
date: 2019-01-01
description: Flyer
client:
  name: Osmiornica Biblioteca
  link: https://osmiornicabiblioteca.com
categories: work
image: /assets/images/work/work-1.jpg
author: Pierina
tags:
  - Artwork
  - Kiterea
---
