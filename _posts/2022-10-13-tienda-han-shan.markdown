---
layout: post-tienda
title: "Han Shan"
date: 2022-10-13
description: zine
categories: shop
image: /assets/images/tienda/han-shan/han-shan-0.png
images-array:
  - image: assets/images/tienda/han-shan/han-shan-0.png
    alt: This is some alt text
  - image: assets/images/tienda/han-shan/han-shan-1.png
    alt: This is some alt text
  - image: assets/images/tienda/han-shan/han-shan-2.png
    alt: This is some alt text
  - image: assets/images/tienda/han-shan/han-shan-3.png
    alt: This is some alt text
  - image: assets/images/tienda/han-shan/han-shan-4.png
    alt: This is some alt text
  - image: assets/images/tienda/han-shan/han-shan-5.png
    alt: This is some alt text
author: Pierina
link-pago: https://mpago.la/1qx8Cvr
precio: 600
tags:
  - Book
  - Kiterea
  - Han Shan
---
  <div class="col soft mb-5">
    <h1>Han Shan</h1>
    <h2>Los poemas de la montaña fría</h2>
  </div>
  <div class="row">
    <div class="col-md-6 soft mb-3">
      <p>
      Han Shan (“montaña fría”, en chino), el ermitaño de la Montaña Fría, “el loco” de la Montaña Fría, lo llamaron.<br>
      Un ser confuso e indescriptible para sus contemporáneos y para la historia acontecida posteriormente, quien como un anacoreta se hundió literalmente en los bosques que cubrían las laderas de la Montaña Fría. Vivió y al parecer murió allí escribiendo en todo lugar posible (peñascos, paredes de acantilados o cortezas de grandes árboles), llevando consigo sólo sus pinceles y tinta, pensamientos, poemas, reflexiones, interrogantes.<br>
      Han Shan, el Monte Frío, más que un lugar, un poeta o una leyenda, sería una poética, un estado de ánimo ligado a la quietud, a la búsqueda solitaria, a la contemplación armoniosa, a la conciencia del vacío:</p>
    </div>

    <div class="col-md-6 soft mb-3">
    <p><i>«Durante al menos dos momentos de los siglos de esplendor Tang, además de la poesía en estricto sentido budista de los monjes-poetas, había al menos una escuela poética de inspiración budista que se movía en la tradición secular y que alcanzó un mérito literario relativo. Los poetas de esa escuela, monjes también, ofrecían la visión de la “montaña helada” (Han Shan) como un estado de ánimo y una búsqueda del Tao».</i><br>

    Octavio Paz. </p>
    </div>

    <div class="col-md-6 soft mb-5 text-work">
    Fanzine <br>
    14,85 x 21 cm <br>
    Tapa: Papel vegetal 100gr <br>
    Interior: Obra de caña natural 80gr <br>
    33 páginas <br>
    Impresión Láser <br>
    Español <br>
    2022
    </div>
  </div>

{% include precio-tienda.html %}
