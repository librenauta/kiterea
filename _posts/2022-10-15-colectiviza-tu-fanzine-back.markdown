---
layout: post
title: "Colectiviza tu fanzine"
date: 2022-10-15
description: Flyer
client:
  name: Librenauta
  link: https://www.copiona.com
categories: work
image: /assets/images/work/portfolio-84(1).png
author: Pierina
tags:
  - Kiterea
  - fanzine
  - colectiviza
---
