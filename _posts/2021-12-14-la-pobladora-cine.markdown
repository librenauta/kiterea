---
layout: post
title: "CARNAVAL DE LAS ALMAS"
date: 2021-12-14
description: Lo gótico y el terror en el cine de Lucrecia Martel
client:
  name: Emmanuel Stichi
  link: https://www.instagram.com/emmabsticchi
categories: work
image: /assets/images/work/portfolio-74.gif
author: Pierina
tags:
  - Kiterea
  - Diseñoeditorial
  - Diseño de tesis
---
